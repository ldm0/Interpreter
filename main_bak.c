#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#define MEM_POOL_LENGTH 10000
#define INPUT_BUFFER_LENGTH 1000
#define VAR_NAME_LENGTH 30
#define VAR_LIST_LENGTH_DEFAULT 4
#define MATCH_FAILED -1
#define TRUE 1
#define FALSE 0
#define ERROR(str) {fputs(#str, stderr); exit(-1);}

const char *type[] = {
	//the int and double are 64 bit
	"int",
	"double",
};

const char *function[] = {
	"print",
};

struct Var {
	//index of const type string
	int type;
	char name[VAR_NAME_LENGTH];
	//all 64bit and currently satellite
	//index of the location in mem_pool
	int index;
};

int			var_list_length;
int			var_num;
struct Var	*var_list;
int			mem_pool_top;
long long	*mem_pool;

int			parse			(char *buffer);
void 		get_words		(char *buffer, int *ptr, char *destination, int length);
int 		func_inside		(int function_index, char *buffer, int *ptr);
int 		match_type		(char *buffer, int *ptr);
int			match_function	(char *buffer, int *ptr);
int			match_var_list	(char *name);
int			match			(char *buffer, int *ptr, const char **possiblities);
void		remove_blank	(char *buffer, int *ptr);

int 
main					(int argc, char *argv[])
{
	char *_input_buffer = (char *)malloc((INPUT_BUFFER_LENGTH + 1) * sizeof(char));
	if (!_input_buffer)
		ERROR(Cannot fuck allocate input buffer!);

	fputs("Nonsense fucking interpreter: 0.0.1 version\n", stdout);

	mem_pool = (long long*)malloc(MEM_POOL_LENGTH * sizeof(long long));
	mem_pool_top = 0;
	if (!mem_pool)
		ERROR(Cannot fuck allocate mem pool!);

	//defualt _length is VAR_LIST_LENGTH_DEFAULT
	//When the var_list is not enough, we realloc to the (var_length * 2)
	var_list_length = VAR_LIST_LENGTH_DEFAULT;
	var_num = 0;
	var_list = (struct Var *)malloc(var_list_length * sizeof(struct Var));
	if (!var_list)
		ERROR(Cannot fuck allocate the var_list);

	for (;;) {
		memset(_input_buffer, 0, INPUT_BUFFER_LENGTH * sizeof(char));
		fputs(">>>", stdout);
		fgets(_input_buffer, INPUT_BUFFER_LENGTH, stdin);
		parse(_input_buffer);
	}

	fputs("exit\n", stdout);

	return 0;
}

int 
parse					(char *buffer)
{
	int ptr = 0;
	remove_blank(buffer, &ptr);
	for (; buffer[ptr]; ++ptr) {
		int _meaning;
		if ((_meaning = match_type(buffer, &ptr)) != MATCH_FAILED) {
			get_words(buffer, &ptr, var_list[var_num].name, VAR_NAME_LENGTH);
			var_list[var_num].type = _meaning;
			var_list[var_num].index = mem_pool_top;
			mem_pool_top++;
			var_num++;
		}
		else if ((_meaning = match_function(buffer, &ptr)) != MATCH_FAILED) {
			if (func_inside(_meaning, buffer, &ptr) == FALSE)
				fputs("function abused!\n", stdout);
		}
		else {
			fputs("cant understand what the fuck you input!\n", stdout);
			return FALSE;
		}
		remove_blank(buffer, &ptr);
	}
	return TRUE;
}

void 
get_words				(char *buffer, int *ptr, char *destination, int length)
{
	remove_blank(buffer, ptr);
	for (int i = 0; buffer[*ptr] && buffer[*ptr] != ' ' && i < length; ++*ptr, ++i) {
		destination[i] = buffer[*ptr];
	}
}

int 
func_inside				(int function_index, char *buffer, int *ptr)
{
	switch (function_index) {
	case 0:
		//print
	{
		char *name = (char *)malloc(VAR_NAME_LENGTH * sizeof(char));
		get_words(buffer, ptr, name, VAR_NAME_LENGTH);
		if (match_var_list(name) == MATCH_FAILED)
			return FALSE;
	}
	break;
	}
}

int 
match_type				(char *buffer, int *ptr)
{
	return match(buffer, ptr, type);
}

int 
match_function			(char *buffer, int *ptr)
{
	return match(buffer, ptr, function);
}

int 
match_var_list			(char *name)
{
	for (int i = 0; i < var_list_length; ++i)
		if (strcmp(var_list[i].name, name) == 0)
			return i;
	return MATCH_FAILED;
}

int 
match					(char *buffer, int *ptr, const char **possibilities)
{
	for (int i = 0; possibilities[i]; ++i)
		for (int j = 0;; ++j) {
			if ((
				!possibilities[i][j]
				&& possibilities[i][j] == buffer[*ptr + j]
				)
				|| buffer[*ptr + j] == ' '){
				*ptr += j;
				return i;
			}
			if (buffer[*ptr + j] != possibilities[i][j])
				break;
		}
	return MATCH_FAILED;
}

void 
remove_blank			(char *buffer, int *ptr)
{
	while (buffer[*ptr] && buffer[*ptr] == ' ')
		++*ptr;
}
