#define MY_DEBUG
#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include<string.h>
#include<vector>


#define MAX_NAME_LENGTH 24
#define VAR_LIST_LENGTH_DEFAULT 8
#define MEM_POOL_LENGTH_DEFAULT 1000
#define INPUT_BUFFER_LENGTH_DEFAULT 512
#define WORDS_BUFFER_LENGTH_DEFAULT 30
#define STACK_LENGTH_DEFAULT 512

#ifndef MY_DEBUG
#define LOG(s)
#define ERR(s)
#else
#define LOG(s) fputs(#s, stdout);
#define ERR(s) fputs(#s, stderr);
#endif



class Words_type {
public:
	enum meanings {
		//From top to bottom, the priority from big to small
		sign,
		key_word,
		data_type,
		function,
		variable,
		_num_meanings
	};
	enum signs {
		sign_add,
		sign_minus,
		sign_mul,
		sign_div,
		sign_equal,
		sign_and,
		sign_or,
		sign_not,
		sign_xor,
		//round left bracket
		sign_rlbracket,
		sign_rrbracket,
		sign_slbracket,
		sign_srbracket,
		sign_clbracket,
		sign_crbracket,
		_num_signs
	};
	enum key_words {
		key_word_if,
		key_word_while,
		_num_key_words
	};
	enum data_types {
		data_type_int,
		data_type_double,
		_num_data_types
	};
	enum functions {
		function_print,
		function_getch,
		_num_functions
	};
private:
	struct _sign{
		char name[5];
	} _signs[_num_signs];
	struct _key_word {
		char name[10];
	} _key_words[_num_key_words];
	struct _data_type {
		size_t _size;
		char name[10];
	} _data_types[_num_data_types];
	struct _function {
		char name[16];
	} _functions[_num_functions];
public:
	Words_type() {
		strcpy(_signs[sign_add].name, "+");
		strcpy(_signs[sign_minus].name, "-");
		strcpy(_signs[sign_mul].name, "*");
		strcpy(_signs[sign_div].name, "/");
		strcpy(_signs[sign_equal].name, "=");
		strcpy(_signs[sign_and].name, "&");
		strcpy(_signs[sign_or].name, "|");
		strcpy(_signs[sign_not].name, "~");
		strcpy(_signs[sign_xor].name, "^");
		strcpy(_signs[sign_rlbracket].name, "(");
		strcpy(_signs[sign_rrbracket].name, ")");
		strcpy(_signs[sign_slbracket].name, "[");
		strcpy(_signs[sign_srbracket].name, "]");
		strcpy(_signs[sign_clbracket].name, "{");
		strcpy(_signs[sign_crbracket].name, "}");
		strcpy(_key_words[key_word_while].name, "while");
		strcpy(_key_words[key_word_if].name, "if");
		_data_types[data_type_int]._size = sizeof(int);
		_data_types[data_type_double]._size = sizeof(double);
		strcpy(_data_types[data_type_int].name, "int");
		strcpy(_data_types[data_type_double].name, "double");
		strcpy(_functions[function_print].name, "print");
		strcpy(_functions[function_getch].name, "getch");
	}
	inline size_t data_type_size(int index) {
		return _data_types[index]._size;
	}
	int match_meaning(char *words, int length) {
		int _tmp_index;
		//if failed return -1, not 0
		//upper 16bits is the type index
		//lower 16bits is the specific index
		if ((_tmp_index = match_signs(words, length)) != -1)
			return _tmp_index | sign << 16;
		if ((_tmp_index = match_key_words(words, length)) != -1)
			return _tmp_index | key_word << 16;
		if ((_tmp_index = match_data_types(words, length)) != -1)
			return _tmp_index | data_type << 16;
		if ((_tmp_index = match_functions(words, length)) != -1)
			return _tmp_index | function << 16;
		return -1;
	}
	int match_signs(char *words, int length) {
		for (int i = 0; i < _num_signs; ++i)
			if (strcmp(words, _signs[i].name) == 0)
				return i;
		return -1;
	}
	int match_key_words(char *words, int length) {
		for (int i = 0; i < _num_key_words; ++i)
			if (strcmp(words, _key_words[i].name) == 0)
				return i;
		return -1;
	}
	int match_data_types(char *words, int length) {
		for (int i = 0; i < _num_data_types; ++i)
			if (strcmp(words, _data_types[i].name) == 0)
				return i;
		return -1;
	}
	int match_functions(char *words, int length) {
		for (int i = 0; i < _num_functions; ++i)
			if (strcmp(words, _functions[i].name) == 0)
				return i;
		return -1;
	}
} g_words_type;

class Mem_pool {
	int _top;
	int _length;
	char *_data;
public:
	Mem_pool() : _top(0) {
		_length = MEM_POOL_LENGTH_DEFAULT;
		_data = (char *)malloc(_length * sizeof(char));
	}
	~Mem_pool() {
		free(_data);
	}
	bool allocate(size_t size) {
		//Now the mem mangment is poor 
		//Later I will implement some complex parallel memory mangement function 
		if (_top + (int)size > _length) {
			_length *= 2;
			_data = (char *)realloc(_data, _length);
		}
	}
	bool release(void *location, size_t size) {
		//The location is virtual address, 
		//tha actual address is the address plus the _data
		//we currently do nothing
		return true;
	}
	long long data_get(void *location, size_t size) {
		//The result is currently largest _data_type
		if (size == sizeof(char))
			return (long long)(*((char *)((long long)location + (long long)_data)));
		if (size == sizeof(int))
			return (long long)(*((int *)((long long)location + (long long)_data)));
		if (size == sizeof(double))
			return *((long long *)((long long)location + (long long)_data));
		return -1;
	}
	bool data_change(void *v_addr, size_t size, long long val) {
		if (!memmove((void *)(((long long)v_addr + (long long)_data)), &val, size))
			return false;
		return true;
	}
};

class Var {
	//index og type[][]
	int					_type;
	//Array support added
	int					_length;
	//point to the mem pool(virtual address)
	class Mem_pool		*_mem_pool;
	void				*_v_addr;
	char				_name[MAX_NAME_LENGTH];
public:
	Var(int t, int l, class Mem_pool *m, void *p, char *n)
		:_type(t), _length(l), _mem_pool(m), _v_addr(p) {
		strncpy(_name, n, MAX_NAME_LENGTH);
	};
	Var(const class Var &var) {
		_type = var._type;
		_length = var._length;
	}
	~Var() { 
		release(); 
	}

	inline int name_cmp(char *name) {
		return strcmp(name, _name);
	}
	inline bool release(void) {
		return _mem_pool->release(_v_addr, _length * g_words_type.data_type_size(_type));
	}
	inline size_t get_size(void) {
		return _length * g_words_type.data_type_size(_type);
	}
	inline size_t get_size(int index) {
		if (index >= _length)
			return -1;
		return g_words_type.data_type_size(_type);
	}
	long long value(void) {
		//use the biggest size of supported types to represent any kind of _data
		return _mem_pool->data_get(_v_addr, g_words_type.data_type_size(_type));
	}
	long long value(int index) {
		return _mem_pool->data_get(
			(char *)_v_addr + _length * g_words_type.data_type_size(_type),
			g_words_type.data_type_size(_type)
		);
	}
	bool value_change(long long val) {
		//if the largest dta type changes to 128bits the long long should change to uint_128
		return _mem_pool->data_change(_v_addr, _length * g_words_type.data_type_size(_type), val);
	}
};

class Var_list {
	std::vector<class Var> _var_list;
public:
	bool add(int type, int length, class Mem_pool *mem_pool, void *pointer, char *name) {
		//if array, _length is more than 1;
		class Var _tmp_var(type, length, mem_pool, pointer, name);
		_var_list.push_back(_tmp_var);
		return true;
	}
	bool remove(char *name) {
		int _index;
		_index = find(name);
		_var_list.erase(_var_list.begin() + _index);
	}
	int find(char *name) {
		int j = _var_list.size();
		for (int i = 0; i < j; ++i)
			if (_var_list[i].name_cmp(name) == 0)
				return i;
		return -1;
	}
};

class Parser {
	int _ptr;
	class Var_list _var_list;
	class Mem_pool _mem_pool;
	class Stack_meaning _stack_meaning;

	//If return false, the buffer to end.
	inline bool _remove_blank(char *buffer, int *ptr) {
		while (buffer[*ptr] == ' ') {
			++*ptr;
			if (buffer[*ptr] == '\n')
				return false;
		}
		return true;
	}
	//If return false, the buffer to end.
	//After getting the words, the ptr move forward.
	inline bool _get_words(char *destination, int length, char *source, int *ptr) {
		if (!_remove_blank(source, ptr))
			return false;
		int i;
		for (i = 0; source[*ptr + i] != '\n' && source[*ptr + i] != ' ' && i < length; ++i) {
			destination[i] = source[*ptr + i];
		}
		*ptr += i;
		return true;
	}

public:
	Parser() : _ptr(0) {};

	bool parse(char *input) {
		int ptr = 0;
		for (; input[ptr] != '\n';)
			if (!parse_init(input, &ptr))
				return false;
		return true;
	}
	bool parse_init(char *input, int *ptr) {
		for (; input[*ptr] != '\n';) {
			class String _words(WORDS_BUFFER_LENGTH_DEFAULT);
			if (!_get_words(_words.string(), _words.length(), input, ptr))
				continue;
			//Trying to get the meaning
			int _match_result = g_words_type.match_meaning(_words.string(), WORDS_BUFFER_LENGTH_DEFAULT);
			//If meaning not found, trying to find if the variable exists.
			if (_match_result == -1) {
				_match_result = _var_list.find(_words.string());
				if (_match_result == -1)
					//If cannot match in parse_init, return false.
					//Because its illegal first word is undeclared variable.
					return false;
				_match_result |= g_words_type.variable << 16;
			}

			int _meaning = _match_result >> 16;
			int _meaning_index = _match_result & 0x0000ffff;

			_stack_meaning.push(_match_result);

			switch (_meaning) {
			case g_words_type.sign:
				//Its impossible to meet sign in parse_init
				//if (!parse_sign(input, ptr, _meaning_index))
				return false;
				break;
			case g_words_type.key_word:
				if (!parse_key_word(input, ptr))
					return false;
				break;
			case g_words_type.data_type:
				if (!parse_data_type(input, ptr))
					return false;
				break;
			case g_words_type.function:
				if (!parse_function(input, ptr))
					return false;
				break;
			case g_words_type.variable:
				if (!parse_variable(input, ptr))
					return false;
				break;
			default:
				return false;
			}

			return true;
		}
		return true;
	}
	bool parse_sign(char *input, int *ptr) {
		if (meaning_index == g_words_type.sign_add)
			LOG(a sign add\n)
		if (meaning_index == g_words_type.sign_and)
			LOG(a sign and\n)
		if (meaning_index == g_words_type.sign_div)
			LOG(a sign div\n)
		if (meaning_index == g_words_type.sign_equal)
			LOG(a sign equal\n)
		if (meaning_index == g_words_type.sign_minus)
			LOG(a sign minus\n)
		if (meaning_index == g_words_type.sign_mul)
			LOG(a sign mul\n);
		if (meaning_index == g_words_type.sign_not)
			LOG(a sign not\n);
		if (meaning_index == g_words_type.sign_or)
			LOG(a sign or\n);
		if (meaning_index == g_words_type.sign_xor)
			LOG(a sign xor\n);
		if (meaning_index == g_words_type.sign_rlbracket)
			LOG(a sign rlbracket\n);
		if (meaning_index == g_words_type.sign_rrbracket)
			LOG(a sign rrbracket\n);
		if (meaning_index == g_words_type.sign_slbracket)
			LOG(a sign slbracket\n);
		if (meaning_index == g_words_type.sign_srbracket)
			LOG(a sign srbracket\n);
		if (meaning_index == g_words_type.sign_clbracket)
			LOG(a sign clbracket\n);
		if (meaning_index == g_words_type.sign_crbracket)
			LOG(a sign crbracket\n);
		return true;
	}
	bool parse_key_word(char *input, int *ptr) {
		char *_words = (char *)malloc(WORDS_BUFFER_LENGTH_DEFAULT * sizeof(char));
		if (!_get_words(_words, WORDS_BUFFER_LENGTH_DEFAULT, input, ptr))
		switch (meaning_index) {
		case g_words_type.key_word_while:
			LOG(a key word while\n);
			break;
		case g_words_type.key_word_if:
			LOG(a key word if\n);
			break;
		default:
			return false;
		}
		return true;
	}
	bool parse_data_type(char *input, int *ptr) {
		if (meaning_index == g_words_type.data_type_double)
			LOG(a data type double\n)
		if (meaning_index == g_words_type.data_type_int)
			LOG(a data type int\n)
		return true;
	}
	bool parse_function(char *input, int *ptr) {
		if (meaning_index == g_words_type.function_getch)
			LOG(a function getch\n);
		if (meaning_index == g_words_type.function_print)
			LOG(a function print\n);
		return true;
	}
	bool parse_variable(char *input, int *ptr) {
		LOG(a declared variable\n);
		return true;
	}
};

class String {
	int _length;
	char *_data;
public:
	String(int length = 256) : _length(length) {
		_data = (char *)malloc(length * sizeof(char));
	}
	String(class String &string) {
		_length = string._length;
		size_t _size = _length *sizeof(char);
		_data = (char *)malloc(_size);
		memset(_data, 0, _length * sizeof(char));
		memcpy(_data, string._data, _size);
	}
	~String() {
		free(_data);
	}

	int length(void) { return _length; }
	//expose the data for scanf or fgets
	char *string(void) { return _data; }
	void clear(void) {
		memset(_data, 0, _length * sizeof(char));
	}
};

class Stack_meaning {
	int _top;
	int _length;
	int *_data;
public:
	Stack_meaning(): _top(0), _length(STACK_LENGTH_DEFAULT) {
		_data = (int *)malloc(STACK_LENGTH_DEFAULT * sizeof(int));
	}
	Stack_meaning(class Stack_meaning &stack) {
		_top = stack._top;
		_length = stack._length;
		size_t _size = _length * sizeof(int);
		_data = (int *)malloc(_size);
		memcpy(_data, stack._data, _size);
	}
	~Stack_meaning() {
		free(_data);
	}

	void push(int data) {
		if (_top >= _length) {
			_length *= 2;
			realloc(_data, _length * sizeof(int));
		}
		_data[_top++] = data;
	}
	bool pop(int *destination) {
		if (_top <= 0)	
			return false;
		*destination = _data[--_top];
		return true;
	}
	bool val_top(int *destination, int offset = 0) {
		if (_top - offset <= 0)	
			return false;
		*destination = _data[_top - offset];
		return true;
	}
	bool val_bottom(int *destination, int index = 0) {
		if (index >= _top)
			return false;
		*destination = _data[index];
		return true;
	}
};

int main()
{
	fputs("none sense fucking interpreter v0.0.1\n", stdout);
	class Parser	_parser;
	class String	_input_buffer(INPUT_BUFFER_LENGTH_DEFAULT);
	for (;;) {
		fputs(">>>", stdout);
		fgets(_input_buffer.string(), _input_buffer.length(), stdin);
		if (!_parser.parse(_input_buffer.string()))
			fputs("What the fuck!\n", stdout);
		_input_buffer.clear();
	}
	return 0;
}
